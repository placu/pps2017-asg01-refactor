package com.geoquiz.controller.account;

/**
 * AccountManager interface that provides methods to manage accounts.
 *
 */
public interface AccountManager {

    /**
     * @param player
     *            the credentials of the player.
     * @return the result of the registration
     */
    boolean register(User player);

    /**
     * @param player
     *            the credentials of the player.
     * @return true if the login was successful, false otherwise
     */
    boolean checkLogin(User player);

    /**
     * Allows the player to exit from his account.
     */
    void logout();

}
