package com.geoquiz.controller.account;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.geoquiz.utility.LocalFolder;

/**
 * Implementation of AccountManager interface.
 *
 */
public final class AccountManagerImpl implements AccountManager, FileOp {

    private final static String FILENAME = "account.txt";
    private final File file;
    private Set<User> users = new HashSet<>();

    private static Optional<User> currentUser = Optional.empty();
    private static AccountManagerImpl instance;

    /**
     * @throws IOException
     *             exception.
     */
    public static AccountManager getAccountManager() throws IOException {
        if (instance == null) {
            instance = new AccountManagerImpl();
        }
        return instance;
    }

    public static Optional<String> getUserLogged() {
        return currentUser.map(User::getUsername);
    }

    /**
     * @throws IOException
     *             exception.
     */
    private AccountManagerImpl() throws IOException {
        this.file = new File(LocalFolder.getLocalFolderPath() + LocalFolder.getFileSeparator() + FILENAME);
        file.createNewFile();
        this.read(this.file.toString());
    }

    private Optional<User> findUser(Predicate<? super User> predicate) {
        return users.stream()
                .filter(predicate)
                .findFirst();
    }

    @Override
    public boolean register(final User player) {
        if(findUser(player::equals).isPresent()) {
            return false;
        }
        users.add(player);
        this.save();
        return true;
    }

    @Override
    public boolean checkLogin(User player) {
        currentUser = findUser(user -> player.equals(user) && player.getPassword().equals(user.getPassword()));
        return currentUser.isPresent();
    }

    @Override
    public void logout() {
        currentUser = Optional.empty();

    }

    @Override
    public void read(final String namefile) throws IOException {

        this.users = Files.lines(Paths.get(namefile)).filter(l -> l.trim().length() > 0).map(l -> l.split(","))
                .map(a -> new UserImpl(a[0].trim(), a[1].trim())).collect(Collectors.toSet());
    }

    @Override
    public void save() {
        PrintStream ps;
        try {
            ps = new PrintStream(this.file);
            users.forEach(p -> ps.println(this.myToString(p)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void removeAllAccount() {

        this.users.clear();
        this.save();

    }

    private String myToString(final User user) {
        return user.getUsername() + "," + user.getPassword();

    }

}
