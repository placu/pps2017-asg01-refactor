package com.geoquiz.controller.account;

public interface User {

    /**
     *
     * @return username of the user
     */
    String getUsername();

    /**
     *
     * @return password of the user
     */
    String getPassword();
}
