package com.geoquiz.controller.account;

import java.util.Objects;

public class UserImpl implements User {

    private final String username;
    private final String password;

    public UserImpl(String username, String password) {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);
        this.username = username;
        this.password = password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        return username.equals(((UserImpl)o).getUsername());
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }
}
