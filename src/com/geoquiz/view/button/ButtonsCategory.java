package com.geoquiz.view.button;

/**
 * The menu's buttons who a player can press to choose category and modality
 * game.
 */
public enum ButtonsCategory {
    /**
     * Represents the category "Capitali".
     */
    CAPITALI,
    /**
     * Represents the category "Valute".
     */
    VALUTE,
    /**
     * Represents the category "Typical dishes".
     */
    CUCINA,
    /**
     * Represents the category "Bandiere".
     */
    BANDIERE,
    /**
     * Represents the category "Monumenti".
     */
    MONUMENTI,
    /**
     * Represents the difficulty level "Facile".
     */
    FACILE,
    /**
     * Represents the difficulty level "Medio".
     */
    MEDIO,
    /**
     * Represents the difficulty level "Difficile".
     */
    DIFFICILE,
    /**
     * Represents the game mode "Classica".
     */
    CLASSICA,
    /**
     * Represents the game mode "Sfida".
     */
    SFIDA,
    /**
     * Represents the game mode "Allenamento".
     */
    ALLENAMENTO;

}
