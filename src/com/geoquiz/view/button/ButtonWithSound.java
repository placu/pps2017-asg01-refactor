package com.geoquiz.view.button;

import com.geoquiz.view.utility.mediaPlayer.SoundPlayer;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.util.function.Consumer;

public class ButtonWithSound extends MyButtonImpl {

    ButtonWithSound(String name, Color colorBackground, double width) {
        super(name, colorBackground, width);
    }

    @Override
    public void setOnMouseClick(Consumer<? super MouseEvent> listener) {
        super.setOnMouseClick((ev) -> {
            SoundPlayer.getClickPlayer().doClickSound();
            listener.accept(ev);
        });
    }
}
