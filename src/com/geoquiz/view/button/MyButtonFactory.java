package com.geoquiz.view.button;

import javafx.scene.paint.Color;

/**
 * static factory for buttons inside menu.
 */
public final class MyButtonFactory {

    private static final double BUTTON_WIDTH = 350;
    private static final double HELPS_WIDTH = 400;
    private static final double ANSWERS_WIDTH = 550;

    private MyButtonFactory() {
    }

    /**
     * create a button with sound when clicked
     * @param name
     *            the text of button.
     *
     * @return the button.
     */
    public static MyButton createButtonWithSound(final String name) {
        return new ButtonWithSound(name, Color.BLUE, BUTTON_WIDTH);
    }

    /**
     *
     * @param name
     *            the text of button.
     *
     * @return the button.
     */
    public static MyButton createMyButton(final String name) {
        return new MyButtonImpl(name, Color.BLUE, BUTTON_WIDTH);
    }

    /**
     *
     * @param name
     *            the text of button.
     *
     * @return the button.
     */
    public static MyButton createAnswerButton(final String name) {
        return new MyButtonImpl(name, Color.BLUE, ANSWERS_WIDTH);
    }

    /**
     *
     * @param name
     *            the text of button.
     *
     * @return the button.
     */
    public static MyButton createHelpButton(final String name) {
        return new MyButtonImpl(name, Color.RED, HELPS_WIDTH);
    }
}
