package com.geoquiz.view.utility;

import java.util.Optional;

public interface OptionQuiz {

    /**
     *
     * @param level of the quiz
     */
    void setLevel(String level);

    /**
     *
     * @return quiz category
     */
    String getCategory();

    /**
     *
     * @return quiz mode
     */
    String getMode();

    /**
     *
     * @return quiz level if present
     */
    Optional<String> getLevel();
}
