package com.geoquiz.view.utility;

import java.util.Optional;

public class OptionQuizImpl implements OptionQuiz {

    private final String category;
    private final String mode;
    private String level;

    public OptionQuizImpl(String category, String mode) {
        this.category = category;
        this.mode = mode;
    }

    @Override
    public void setLevel(String level) {
        this.level = level;
    }

    @Override
    public String getCategory() {
        return category;
    }

    @Override
    public String getMode() {
        return mode;
    }

    @Override
    public Optional<String> getLevel() {
        return Optional.ofNullable(level);
    }
}
