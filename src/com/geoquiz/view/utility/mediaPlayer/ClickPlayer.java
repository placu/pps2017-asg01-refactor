package com.geoquiz.view.utility.mediaPlayer;

public interface ClickPlayer {
    /**
     * If enable do a click sound
     */
    void doClickSound();
}
