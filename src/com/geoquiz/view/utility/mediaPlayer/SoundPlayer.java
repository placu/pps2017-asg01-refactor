package com.geoquiz.view.utility.mediaPlayer;

import com.geoquiz.utility.ResourceLoader;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class SoundPlayer implements OptionPlayer, ClickPlayer {

    private final MediaPlayer geoQuizMusic;
    private final MediaPlayer click;
    private boolean soundOn;
    private boolean clickOn;

    private static SoundPlayer player;

    public static ClickPlayer getClickPlayer() {
        if(player == null) {
            player = new SoundPlayer();
        }
        return player;
    }

    public static OptionPlayer getOptionPlayer() {
        if(player == null) {
            player = new SoundPlayer();
        }
        return player;
    }

    private SoundPlayer() {
        soundOn = true;
        clickOn = true;
        click = new MediaPlayer(new Media(ResourceLoader.loadResource("/media/click.wav").toExternalForm()));
        geoQuizMusic = new MediaPlayer(new Media(ResourceLoader.loadResource("/media/geoMusic.mp3").toString()));
        geoQuizMusic.setVolume(geoQuizMusic.getVolume() / 2);
        geoQuizMusic.setCycleCount(MediaPlayer.INDEFINITE);
        geoQuizMusic.setAutoPlay(true);
    }

    @Override
    public void doClickSound() {
        if(isWavEnabled()) {
            playClick();
        }
    }

    @Override
    public void disableMusic() {
        soundOn = false;
        geoQuizMusic.stop();
    }

    @Override
    public void resumeMusic() {
        soundOn = true;
        geoQuizMusic.play();
    }

    @Override
    public void playClick() {
        clickOn = true;
        click.stop();
        click.play();
    }

    @Override
    public void stopClick() {
        clickOn = false;
        click.stop();
        click.play();
    }

    @Override
    public boolean isMusicEnabled() {
        return soundOn;
    }

    @Override
    public boolean isWavEnabled() {
        return clickOn;
    }

}
