package com.geoquiz.view.utility.mediaPlayer;

public interface OptionPlayer {
    /**
     * Method to stop background music.
     */
    void disableMusic();

    /**
     * Method to resume background music.
     */
    void resumeMusic();

    /**
     * Method to play click sound.
     */
    void playClick();

    /**
     * Method to stop click sound.
     */
    void stopClick();

    /**
     * Method to know if music is disabled.
     *
     * @return true if music is off, else true.
     */
    boolean isMusicEnabled();

    /**
     * Method to know if sound click is disabled.
     *
     * @return true if sound click is off, else true.
     */
    boolean isWavEnabled();
}
