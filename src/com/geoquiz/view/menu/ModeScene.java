package com.geoquiz.view.menu;

import com.geoquiz.view.utility.Background;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.button.ButtonsCategory;
import com.geoquiz.view.button.MyButton;
import com.geoquiz.view.button.MyButtonFactory;
import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelFactory;

import com.geoquiz.view.utility.OptionQuiz;
import com.geoquiz.view.utility.OptionQuizImpl;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * The scene where user can choose game modality.
 */
public class ModeScene extends Scene {

    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 350;
    private static final double POS_3_Y = 200;
    private static final double POS_Y_BACK = 600;
    private static final double POS_1_X = 100;
    private static final double LABEL_FONT = 35;
    private static final double OPACITY = 0.5;

    private final Pane panel = new Pane();
    private final VBox vbox = new VBox();
    private final VBox vbox2 = new VBox();
    private final VBox vbox3 = new VBox();
    private final Label label = new Label();
    private final Stage mainStage;
    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public ModeScene(final Stage mainStage, final String category) {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());
        this.mainStage = mainStage;
        final MyButton back;
        final MyButton classic;
        final MyButton challenge;
        final MyButton training;

        final MyLabel userLabel = MyLabelFactory.createUserLabel();

        back = MyButtonFactory.createButtonWithSound(Buttons.INDIETRO.toString());
        classic = MyButtonFactory.createButtonWithSound(ButtonsCategory.CLASSICA.toString());
        challenge = MyButtonFactory.createButtonWithSound(ButtonsCategory.SFIDA.toString());
        training = MyButtonFactory.createButtonWithSound(ButtonsCategory.ALLENAMENTO.toString());

        if (category.equals(ButtonsCategory.CAPITALI.toString())) {
            label.setText("Sai indicare la capitale di ciascun paese?\nScegli prima la modalità di gioco!");
        } else if (category.equals(ButtonsCategory.MONUMENTI.toString())) {
            label.setText(
                    "Sai indicacare dove si trovano questi famosi monumenti?\nScegli prima la modalità di gioco!");
        } else if (category.equals(ButtonsCategory.BANDIERE.toString())) {
            label.setText("Sai indicare i paesi in base alla bandiera nazionale?\nScegli prima la modalità di gioco!");
        } else if (category.equals(ButtonsCategory.CUCINA.toString())) {
            label.setText("Sai indicare di quali paesi sono tipici questi piatti?\nScegli prima la modalità di gioco!");
        } else if (category.equals(ButtonsCategory.VALUTE.toString())) {
            label.setText(
                    "Sai indicare qual e' la valuta adottata da ciascun paese?\nScegli prima la modalità di gioco!");
        }

        label.setFont(Font.font("Italic", FontWeight.BOLD, LABEL_FONT));

        vbox.setTranslateX(POS_2_X);
        vbox.setTranslateY(POS_2_Y);
        vbox.getChildren().addAll((Node) classic, (Node) challenge, (Node) training);
        vbox2.getChildren().add((Node) back);
        vbox3.getChildren().add(label);

        vbox2.setTranslateX(POS_1_X);
        vbox2.setTranslateY(POS_Y_BACK);
        vbox3.setTranslateX(POS_2_X);
        vbox3.setTranslateY(POS_3_Y);

        back.setOnMouseClick(event -> mainStage.setScene(new CategoryScene(mainStage)));

        classic.setOnMouseClick(event -> {
            OptionQuiz optionQuiz = new OptionQuizImpl(category, ButtonsCategory.CLASSICA.toString());
            if (category.equals(ButtonsCategory.CAPITALI.toString())
                    || category.equals(ButtonsCategory.MONUMENTI.toString())) {
                mainStage.setScene(new LevelScene(mainStage, optionQuiz));
            } else {
                openQuizGamePlay(optionQuiz);
            }
        });

        challenge.setOnMouseClick(event -> openQuizGamePlay(new OptionQuizImpl(category, ButtonsCategory.SFIDA.toString())));

        training.setOnMouseClick(event -> openQuizGamePlay(new OptionQuizImpl(category, ButtonsCategory.ALLENAMENTO.toString())));

        this.panel.getChildren().addAll(Background.createBackgroundImageForCategory(category), Background.createBackground(),
                Background.getLogo(), vbox, vbox2, vbox3, (Node) userLabel);

        this.setRoot(this.panel);

    }

    private void openQuizGamePlay(OptionQuiz optionQuiz) {
        try {
            mainStage.setScene(new QuizGamePlay(mainStage, optionQuiz));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
