package com.geoquiz.view.menu;

import java.io.IOException;

import com.geoquiz.controller.account.AccountManager;
import com.geoquiz.controller.account.AccountManagerImpl;
import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.button.MyButtonFactory;
import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelFactory;
import com.geoquiz.view.menu.ranking.AbsoluteRankingScene;
import com.geoquiz.view.menu.ranking.MyRankingScene;
import com.geoquiz.view.utility.Background;
import com.geoquiz.view.utility.ExitProgram;
import com.geoquiz.view.button.MyButton;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * The scene where user can choose how to do.
 */
public class MainMenuScene extends Scene {

    private static final double POS_1_X = 100;
    private static final double POS_1_Y = 450;
    private static final double BUTTON_WIDTH = 350;
    private static final double USER_LABEL_FONT = 40;
    private static final double POS_X_INSTRUCTIONS = 900;
    private static final double POS_Y_INSTRUCTIONS = 638;

    private final Pane panel = new Pane();
    private final VBox vbox = new VBox();
    private final VBox instructionsButtonBox = new VBox();

    /**
     * @param mainStage
     *            the stage where the scene is called.
     * @throws IOException
     *             for exception.
     */
    public MainMenuScene(final Stage mainStage) throws IOException {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());

        final MyButton play;
        final MyButton ranking;
        final MyButton options;
        final MyButton exit;
        final MyButton stats;
        final MyButton instructions;
        final AccountManager accountManager = AccountManagerImpl.getAccountManager();

        final MyLabel userLabel = MyLabelFactory.createUserLabel();

        play = MyButtonFactory.createButtonWithSound(Buttons.GIOCA.toString());
        ranking = MyButtonFactory.createButtonWithSound(Buttons.CLASSIFICA.toString());
        options = MyButtonFactory.createButtonWithSound(Buttons.OPZIONI.toString());
        exit = MyButtonFactory.createButtonWithSound(Buttons.ESCI.toString());
        stats = MyButtonFactory.createButtonWithSound(Buttons.STATISTICHE.toString());
        instructions = MyButtonFactory.createButtonWithSound(Buttons.ISTRUZIONI.toString());

        instructionsButtonBox.setTranslateX(POS_X_INSTRUCTIONS);
        instructionsButtonBox.setTranslateY(POS_Y_INSTRUCTIONS);
        instructionsButtonBox.getChildren().add((Node) instructions);

        vbox.setTranslateX(POS_1_X);
        vbox.setTranslateY(POS_1_Y);
        vbox.getChildren().addAll((Node) play, (Node) stats, (Node) ranking, (Node) options, (Node) exit);

        exit.setOnMouseClick(event -> {
            accountManager.logout();
            ExitProgram.exitProgram(mainStage);
        });

        instructions.setOnMouseClick(event -> mainStage.setScene(new InstructionScene(mainStage)));

        stats.setOnMouseClick(event -> mainStage.setScene(new MyRankingScene(mainStage)));

        ranking.setOnMouseClick(event -> mainStage.setScene(new AbsoluteRankingScene(mainStage)));

        options.setOnMouseClick(event -> mainStage.setScene(new OptionScene(mainStage)));

        play.setOnMouseClick(event -> mainStage.setScene(new CategoryScene(mainStage)));

        this.panel.getChildren().addAll(Background.getImage(), Background.createBackground(), vbox,
                Background.getLogo(), (Node) userLabel, instructionsButtonBox);

        this.setRoot(this.panel);
    }

}
