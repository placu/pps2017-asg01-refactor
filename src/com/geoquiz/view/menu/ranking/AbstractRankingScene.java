package com.geoquiz.view.menu.ranking;

import com.geoquiz.controller.ranking.Ranking;
import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.button.ButtonsCategory;
import com.geoquiz.view.button.MyButton;
import com.geoquiz.view.button.MyButtonFactory;
import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelFactory;
import com.geoquiz.view.menu.MainMenuScene;
import com.geoquiz.view.utility.Background;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;

public abstract class AbstractRankingScene extends Scene {
    private static final double POS_1_X = 20;
    private static final double POS_Y_BACK = 650;
    private static final double FONT = 35;
    protected static final double BUTTON_WIDTH = 350;
    private static final double FONT_MODE = 25;
    private static final double POS_X_CATEGORY_BOX = 50;
    private static final double POS_Y_CATEGORY_BOX = 200;
    private static final double POS_X_CATEGORY_BOX_2 = 650;
    private static final double POS_Y_CATEGORY_BOX_2 = 75;
    private static final double POS_X_CAPITALS_BOX = 300;
    private static final double POS_Y_CAPITALS_BOX = 205;
    private static final double POS_X_MONUMENTS_BOX = 300;
    private static final double POS_Y_MONUMENTS_BOX = 450;
    private static final double POS_X_FLAGS_BOX = 850;
    private static final double POS_Y_FLAGS_BOX = 80;
    private static final double POS_X_CURRENCIES_BOX = 850;
    private static final double POS_Y_CURRENCIES_BOX = 325;
    private static final double POS_X_DISHES_BOX = 850;
    private static final double POS_Y_DISHES_BOX = 570;
    private static final double TITLE_FONT = 95;
    private static final String S = " -> ";
    protected final Pane panel = new Pane();
    protected final VBox vbox = new VBox();
    private final VBox categoryBox = new VBox(200);
    private final VBox categoryBox2 = new VBox(200);
    private final VBox capitalsBox = new VBox();
    private final VBox monumentsBox = new VBox();
    private final VBox dishesBox = new VBox();
    private final VBox currenciesBox = new VBox();
    private final VBox flagsBox = new VBox();
    private final VBox titleBox = new VBox();
    private final MyLabel title;
    private final MyLabel capitalsEasy;
    private final MyLabel capitalsMedium;
    private final MyLabel capitalsHard;
    private final MyLabel capitalsChallenge;
    private final MyLabel monumentsEasy;
    private final MyLabel monumentsMedium;
    private final MyLabel monumentsHard;
    private final MyLabel monumentsChallenge;
    private final MyLabel flagsClassic;
    private final MyLabel flagsChallenge;
    private final MyLabel currenciesClassic;
    private final MyLabel currenciesChallenge;
    private final MyLabel dishesClassic;
    private final MyLabel dishesChallenge;

    public AbstractRankingScene(final Stage mainStage, String title) {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());

        final MyLabel capitals;
        final MyLabel monuments;
        final MyLabel flags;
        final MyLabel currencies;
        final MyLabel dishes;

        final MyButton indietro = MyButtonFactory.createButtonWithSound(Buttons.INDIETRO.toString());

        this.title = MyLabelFactory.createMyLabel(title, Color.BLACK, TITLE_FONT);

        capitals = createCategoryLabel(ButtonsCategory.CAPITALI.toString());
        monuments = createCategoryLabel(ButtonsCategory.MONUMENTI.toString());
        flags = createCategoryLabel(ButtonsCategory.BANDIERE.toString());
        currencies = createCategoryLabel(ButtonsCategory.VALUTE.toString());
        dishes = createCategoryLabel(ButtonsCategory.CUCINA.toString());

        initializeRanking(Ranking.getInstance());

        capitalsEasy = createRecordLabel(ButtonsCategory.CAPITALI.toString(), ButtonsCategory.FACILE.toString());
        capitalsMedium = createRecordLabel(ButtonsCategory.CAPITALI.toString(), ButtonsCategory.MEDIO.toString());
        capitalsHard = createRecordLabel(ButtonsCategory.CAPITALI.toString(), ButtonsCategory.DIFFICILE.toString());
        capitalsChallenge = createRecordLabel(ButtonsCategory.CAPITALI.toString(), ButtonsCategory.SFIDA.toString());
        monumentsEasy = createRecordLabel(ButtonsCategory.MONUMENTI.toString(), ButtonsCategory.FACILE.toString());
        monumentsMedium = createRecordLabel(ButtonsCategory.MONUMENTI.toString(), ButtonsCategory.MEDIO.toString());
        monumentsHard = createRecordLabel(ButtonsCategory.MONUMENTI.toString(), ButtonsCategory.DIFFICILE.toString());
        monumentsChallenge = createRecordLabel(ButtonsCategory.MONUMENTI.toString(), ButtonsCategory.SFIDA.toString());
        flagsClassic = createRecordLabel(ButtonsCategory.BANDIERE.toString(), ButtonsCategory.CLASSICA.toString());
        flagsChallenge = createRecordLabel(ButtonsCategory.BANDIERE.toString(), ButtonsCategory.SFIDA.toString());
        currenciesClassic = createRecordLabel(ButtonsCategory.VALUTE.toString(), ButtonsCategory.CLASSICA.toString());
        currenciesChallenge = createRecordLabel(ButtonsCategory.VALUTE.toString(), ButtonsCategory.SFIDA.toString());
        dishesClassic = createRecordLabel(ButtonsCategory.CUCINA.toString(), ButtonsCategory.CLASSICA.toString());
        dishesChallenge = createRecordLabel(ButtonsCategory.CUCINA.toString(), ButtonsCategory.SFIDA.toString());

        titleBox.getChildren().add((Node) this.title);

        capitalsBox.getChildren().addAll((Node) capitalsEasy, (Node) capitalsMedium, (Node) capitalsHard,
                (Node) capitalsChallenge);
        monumentsBox.getChildren().addAll((Node) monumentsEasy, (Node) monumentsMedium, (Node) monumentsHard,
                (Node) monumentsChallenge);
        currenciesBox.getChildren().addAll((Node) currenciesClassic, (Node) currenciesChallenge);
        flagsBox.getChildren().addAll((Node) flagsClassic, (Node) flagsChallenge);
        dishesBox.getChildren().addAll((Node) dishesClassic, (Node) dishesChallenge);

        categoryBox.getChildren().addAll((Node) capitals, (Node) monuments);
        categoryBox2.getChildren().addAll((Node) flags, (Node) currencies, (Node) dishes);

        categoryBox.setTranslateX(POS_X_CATEGORY_BOX);
        categoryBox.setTranslateY(POS_Y_CATEGORY_BOX);
        categoryBox2.setTranslateX(POS_X_CATEGORY_BOX_2);
        categoryBox2.setTranslateY(POS_Y_CATEGORY_BOX_2);
        capitalsBox.setTranslateX(POS_X_CAPITALS_BOX);
        capitalsBox.setTranslateY(POS_Y_CAPITALS_BOX);
        monumentsBox.setTranslateX(POS_X_MONUMENTS_BOX);
        monumentsBox.setTranslateY(POS_Y_MONUMENTS_BOX);
        dishesBox.setTranslateX(POS_X_DISHES_BOX);
        dishesBox.setTranslateY(POS_Y_DISHES_BOX);
        flagsBox.setTranslateX(POS_X_FLAGS_BOX);
        flagsBox.setTranslateY(POS_Y_FLAGS_BOX);
        currenciesBox.setTranslateX(POS_X_CURRENCIES_BOX);
        currenciesBox.setTranslateY(POS_Y_CURRENCIES_BOX);

        vbox.setTranslateX(POS_1_X);
        vbox.setTranslateY(POS_Y_BACK);
        vbox.getChildren().add((Node) indietro);

        indietro.setOnMouseClick(event -> {
            try {
                mainStage.setScene(new MainMenuScene(mainStage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        this.panel.getChildren().addAll(Background.getImage(), Background.createBackground(), vbox, categoryBox,
                categoryBox2, capitalsBox, monumentsBox, flagsBox, currenciesBox, dishesBox, titleBox);

        this.setRoot(this.panel);
    }

    private MyLabel createRecordLabel(String category, String difficulty) {
        return MyLabelFactory.createMyLabel(difficulty + S + this.getRecordByCategory(category, difficulty),
                Color.BLACK, FONT_MODE);
    }

    private MyLabel createCategoryLabel(String category) {
        return MyLabelFactory.createMyLabel(category, Color.RED, FONT);
    }

    protected abstract String getRecordByCategory(String category, String difficulty);

    protected abstract void initializeRanking(Ranking ranking);
}
