package com.geoquiz.view.menu.ranking;

import java.io.IOException;
import java.util.Map;

import javax.xml.bind.JAXBException;

import com.geoquiz.controller.account.AccountManagerImpl;
import com.geoquiz.controller.ranking.Ranking;
import com.geoquiz.utility.Pair;

import javafx.stage.Stage;

/**
 * The statistics scene where user can see own records.
 */
public class MyRankingScene extends AbstractRankingScene {

    private Map<Pair<String, String>, Integer> map;

    /**
     * @param mainStage
     *            the stage where the scene is called.
     * @throws JAXBException
     *             for xml exception.
     */
    public MyRankingScene(final Stage mainStage) {
        super(mainStage, "My records");
    }

    @Override
    protected String getRecordByCategory(final String category, final String difficulty) {
        final Integer record = this.map.get(new Pair<>(category, difficulty));
        return record == null ? "" : record.toString();
    }

    @Override
    protected void initializeRanking(Ranking ranking) {
        try {
            map = ranking.getPersonalRanking(AccountManagerImpl.getUserLogged().orElse(""));
        } catch (ClassNotFoundException | IOException e1) {
            e1.printStackTrace();
        }
    }
}