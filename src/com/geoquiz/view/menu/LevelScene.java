package com.geoquiz.view.menu;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.button.ButtonsCategory;
import com.geoquiz.view.button.MyButtonFactory;
import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelFactory;
import com.geoquiz.view.utility.Background;
import com.geoquiz.view.button.MyButton;
import com.geoquiz.view.utility.OptionQuiz;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * The scene where user can choose difficulty level.
 */
public class LevelScene extends Scene {

    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 300;
    private static final double POS_Y_BACK = 600;
    private static final double POS_1_X = 100;

    private final Pane panel = new Pane();
    private final VBox vbox = new VBox();
    private final VBox vbox2 = new VBox();
    private final Stage mainStage;
    private final OptionQuiz optionQuiz;

    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public LevelScene(final Stage mainStage, final OptionQuiz optionQuiz) {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());
        this.mainStage = mainStage;
        this.optionQuiz = optionQuiz;
        final MyLabel userLabel = MyLabelFactory.createUserLabel();

        final MyButton back;
        final MyButton easy;
        final MyButton medium;
        final MyButton hard;

        back = MyButtonFactory.createButtonWithSound(Buttons.INDIETRO.toString());
        easy = MyButtonFactory.createButtonWithSound(ButtonsCategory.FACILE.toString());
        medium = MyButtonFactory.createButtonWithSound(ButtonsCategory.MEDIO.toString());
        hard = MyButtonFactory.createButtonWithSound(ButtonsCategory.DIFFICILE.toString());

        vbox.getChildren().addAll((Node) easy, (Node) medium, (Node) hard);
        vbox2.getChildren().add((Node) back);

        vbox.setTranslateX(POS_2_X);
        vbox.setTranslateY(POS_2_Y);

        vbox2.setTranslateX(POS_1_X);
        vbox2.setTranslateY(POS_Y_BACK);

        back.setOnMouseClick(event -> mainStage.setScene(new CategoryScene(mainStage)));

        easy.setOnMouseClick(event -> openQuizGamePlay(ButtonsCategory.FACILE.toString()));

        medium.setOnMouseClick(event -> openQuizGamePlay(ButtonsCategory.MEDIO.toString()));

        hard.setOnMouseClick(event -> openQuizGamePlay(ButtonsCategory.DIFFICILE.toString()));

        this.panel.getChildren().addAll(Background.createBackgroundImageForCategory(optionQuiz.getCategory()),
                Background.createBackground(), vbox, vbox2,
                Background.getLogo(), (Node) userLabel);

        this.setRoot(this.panel);
    }

    private void openQuizGamePlay(String level) {
        optionQuiz.setLevel(level);
        try {
            mainStage.setScene(new QuizGamePlay(mainStage, optionQuiz));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
