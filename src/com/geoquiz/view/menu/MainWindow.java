package com.geoquiz.view.menu;

import java.io.IOException;

import com.geoquiz.utility.ResourceLoader;
import com.geoquiz.view.utility.ScreenAdapter;

import javafx.animation.PauseTransition;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

/**
 * Main window of game.
 */
public class MainWindow extends Application {

    private static final String FILE = "/images/image.jpg";

    private final SplashScreen splashScreen = new SplashScreen();

    /**
     * @param primaryStage
     *            the principal stage.
     */
    public void start(final Stage primaryStage) {

        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.setWidth(ScreenAdapter.getScreenWidth());
        primaryStage.setHeight(ScreenAdapter.getScreenHeight());
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image(ResourceLoader.loadResourceAsStream(FILE)));
        try {
            primaryStage.setScene(new LoginMenuScene(primaryStage));
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        splashScreen.showWindow();
        final PauseTransition splashScreenDelay = new PauseTransition(Duration.seconds(2));
        splashScreenDelay.setOnFinished(e -> {
            primaryStage.show();
            splashScreen.hideWindow();
        });
        splashScreenDelay.playFromStart();
    }
}
