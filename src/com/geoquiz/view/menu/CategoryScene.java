package com.geoquiz.view.menu;

import com.geoquiz.view.utility.Background;

import java.io.IOException;

import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.button.ButtonsCategory;
import com.geoquiz.view.button.MyButton;
import com.geoquiz.view.button.MyButtonFactory;
import com.geoquiz.view.label.MyLabel;
import com.geoquiz.view.label.MyLabelFactory;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * The scene where user can choose game category.
 */
public class CategoryScene extends Scene {

    private static final double POS_1_X = 100;
    private static final double POS_1_Y = 450;
    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 300;
    private static final double POS_X_BACK = 450;
    private static final double POS_Y_BACK = 600;

    private final Pane panel = new Pane();

    private final HBox hbox = new HBox(10);
    private final HBox hbox2 = new HBox(10);
    private final VBox vbox = new VBox();

    private final Stage mainStage;
    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public CategoryScene(final Stage mainStage) {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());
        this.mainStage = mainStage;
        final MyButton capitals;
        final MyButton currencies;
        final MyButton dishes;
        final MyButton monuments;
        final MyButton flags;
        final MyButton back;

        final MyLabel userLabel = MyLabelFactory.createUserLabel();

        capitals = MyButtonFactory.createButtonWithSound(ButtonsCategory.CAPITALI.toString());
        currencies = MyButtonFactory.createButtonWithSound(ButtonsCategory.VALUTE.toString());
        dishes = MyButtonFactory.createButtonWithSound(ButtonsCategory.CUCINA.toString());
        monuments = MyButtonFactory.createButtonWithSound(ButtonsCategory.MONUMENTI.toString());
        flags = MyButtonFactory.createButtonWithSound(ButtonsCategory.BANDIERE.toString());
        back = MyButtonFactory.createButtonWithSound(Buttons.INDIETRO.toString());

        hbox.setTranslateX(POS_1_X);
        hbox.setTranslateY(POS_1_Y);

        hbox2.setTranslateX(POS_2_X);
        hbox2.setTranslateY(POS_2_Y);

        vbox.setTranslateX(POS_X_BACK);
        vbox.setTranslateY(POS_Y_BACK);

        hbox.getChildren().addAll((Node) flags, (Node) currencies, (Node) dishes);
        hbox2.getChildren().addAll((Node) monuments, (Node) capitals);
        vbox.getChildren().add((Node) back);

        back.setOnMouseClick(event -> {
            try {
                mainStage.setScene(new MainMenuScene(mainStage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        capitals.setOnMouseClick(event -> openModeScene(ButtonsCategory.CAPITALI.toString()));

        monuments.setOnMouseClick(event -> openModeScene(ButtonsCategory.MONUMENTI.toString()));

        currencies.setOnMouseClick(event -> openModeScene(ButtonsCategory.VALUTE.toString()));

        flags.setOnMouseClick(event -> openModeScene(ButtonsCategory.BANDIERE.toString()));

        dishes.setOnMouseClick(event -> openModeScene(ButtonsCategory.CUCINA.toString()));

        this.panel.getChildren().addAll(Background.getImage(), Background.createBackground(), hbox, hbox2, vbox,
                Background.getLogo(), (Node) userLabel);

        this.setRoot(this.panel);
    }

    private void openModeScene(String category) {
        mainStage.setScene(new ModeScene(mainStage, category));
    }
}
